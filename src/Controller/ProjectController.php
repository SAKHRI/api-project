<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Project;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;




/**
 * @Route("/rest/project", name="project")
 */

class ProjectController extends Controller
{

    private $serializer;
    //On injecte le serializer dans le constructeur de la classe
    //contrôleur car on devra s'en servir dans toutes les méthodes
    //de celle ci.
    public function __construct(SerializerInterface $serializer) 
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/", methods={"GET"})
     */
    public function index()
    {
        // //On crée une réponse http
        // $response = new Response();
        // //On indique que son contenu sera du json
        // $response->headers->set("Content-Type", "application/json");
        // //On indique que le code de retour est 200 ok
        // $response->setStatusCode(200);
        // //On met comme contenu un petit tableau encodé en json
        // $response->setContent(json_encode(["ga" => "bu"]));
        
        $repo = $this->getDoctrine()->getRepository(Project::class);
        
        //On utilise le serializer de symfony pour transfomer une
        //entité (ou array d'entités) php au format json
        $json = $this->serializer->serialize($repo->findAll(), "json");
        //On utilise la méthode static fromJsonString de la classe
        //JsonResponse pour créer une réponse HTTP en json à partir
        //de données déjà au format JSON
        return JsonResponse::fromJsonString($json);
    }
    
    /**
     * @Route("/", methods={"POST"})
     */
    public function add(Request $request) {
        //On récupère le body de la request (les données de celle ci)
        $body = $request->getContent();
        
        //On transforme ce body en instance de Student depuis le format json
        $project = $this->serializer->deserialize($body, Project::class, "json");
        $data = json_decode($body, true);
        $project->setBase64Image($data["imageBase64"]);
        //On fait persister le student via doctrine
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($project);
        $manager->flush();
        //On envoie une réponse pour dire que c'est ok et dire l'id qui vient d'être ajouté
        return new JsonResponse(["id" => $project->getId()]);
    }

    /**
     * @Route("/{project}", methods={"GET"})
     */
    public function single(Project $project) {
        $json = $this->serializer->serialize($project,"json");
        return JsonResponse::fromJsonString($json);
    }

    /**
     * @Route("/{project}", methods={"DELETE"})
     */
    public function remove(Project $project) {
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($project);
        $manager->flush();
        return new Response("", 204);
    }

    /**
     * @Route("/{project}", methods={"PUT"})
     */
    public function update(Project $project, Request $request) {
        $body = $request->getContent();
        $updated = $this->serializer->deserialize($body, Project::class, "json");

        $manager = $this->getDoctrine()->getManager();

        $project->setName($updated->getName());
        $project->setDescription($updated->getDescription());
        $project->setImage($updated->setBase64Image($updated->getImage()));
        $project->setTechno($updated->getTechno());
        $project->setUrlGitlab($updated->geturlGitlab());
        
        $manager->persist($project);
        $manager->flush();
        return new Response("", 204);
    }


    

     /**
     * @Route("/{project}", methods={"PATCH"})
     */
    public function singleupdate(Project $project, Request $request)
    {
        
        $manager = $this->getDoctrine()->getManager();
        $body = json_decode($request->getContent(), true);

        foreach ($body as $key => $value) {
            $project->{'set' . ucfirst($key)}($value);
        }
        
        $manager->persist($project);
        
        $manager->flush();
        // we can customize the data format returned by mapping it in an array (here NORMALIZER_FORMAT)
        // see https://symfony.com/doc/current/components/serializer.html#selecting-specific-attributes
        $data = $this->serializer->normalize($project, null, self::NORMALIZER_FORMAT);
        // convert formated datas to json using serialize()
        $json = $this->serializer->serialize($data, 'json');
        // prepare response object
        $response = new Response($json);
        // setup response headers
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

}